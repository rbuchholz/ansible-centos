# This file is managed by Ansible. Don't make changes here, they will be overwritten.

# check syntax:
# exim -C /etc/exim/exim.conf -bV

.include /etc/exim/role.conf
.include_if_exists /etc/exim/local.conf

# The option "relay_to_domains" is useful when migrating to a new mail server.
# The old one knows about the new server and can forward messages to the new one
# until all (well-behaved) senders see the new DNS.
.ifndef
    domainlist relay_to_domains =
.endif
hostlist   relay_from_hosts = 127.0.0.1 : ::::1/128


# from the Exim documentation:
# > When Exim receives messages over SMTP connections, it expects all addresses
# > to be fully qualified with a domain, as required by the SMTP definition.
# ...
# > [...] you can specify hosts that are permitted to send unqualified sender
# > and recipient addresses, respectively.
#
# yum-cron might use unqualified addresses (depending on its configuration)
sender_unqualified_hosts = 127.0.0.1 : ::::1/128
recipient_unqualified_hosts = 127.0.0.1 : ::::1/128

acl_smtp_mail = acl_check_mail
acl_smtp_rcpt = acl_check_rcpt
acl_smtp_mime = acl_check_mime

tls_advertise_hosts = *
tls_certificate = /etc/exim/tls/fullchain.pem
tls_privatekey = /etc/exim/tls/privkey.pem
# configuration	according to recommendations from:
# "Applied Crypto Hardening" -- Draft revision: 792d4bd (2015-12-08)
# source: https://bettercrypto.org/static/applied-crypto-hardening.pdf
#
# > It is not advisable to restrict the default cipher list for opportunistic
# > encryption as used by SMTP. Do not use cipher lists recommended for HTTPS!
# (...)
# > Do not limit ciphers without a very good reason. In the worst case you end up
# > If without encryption at all instead of some weak encryption.
#
# > Note: +all is misleading here since OpenSSL only activates the most common
# > workarounds. But that's how SSL_OP_ALL is defined.
#
# > You do not need to set dh_parameters. Exim with OpenSSL by default uses
# > parameter initialization with the "2048-bit MODP Group with 224-bit Prime
# > Order Subgroup" defined in section 2.2 of RFC 5114
openssl_options = +all +no_sslv2 +no_compression +cipher_server_preference

daemon_smtp_ports = 25 : 465 : 587
# SMTPS: required for Outlook 2011 on Mac and similar old clients
# (also helpful for simpler testing with the openssl command-line client)
tls_on_connect_ports = 465

# qualify_domain =
never_users = root
host_lookup = 
auth_advertise_hosts = ${if eq {$tls_cipher}{}{}{*}}
rfc1413_hosts = 

ignore_bounce_errors_after = 2d
timeout_frozen_after = 4d

# prevent Exim warning "WARNING: purging the environment." (related to CVE-2016-1531)
keep_environment =


######################################################################
#                       ACL CONFIGURATION                            #
#         Specifies access control lists for incoming SMTP mail      #
######################################################################

begin acl

.include /etc/exim/acls.conf


######################################################################
#                      ROUTERS CONFIGURATION                         #
#               Specifies how addresses are handled                  #
######################################################################

begin routers

.include /etc/exim/routers.conf


######################################################################
#                      TRANSPORTS CONFIGURATION                      #
######################################################################

begin transports

.include /etc/exim/transports.conf

######################################################################
#                      RETRY CONFIGURATION                           #
######################################################################

begin retry

# This single retry rule applies to all domains and all errors. It specifies
# retries every 15 minutes for 2 hours, then increasing retry intervals,
# starting at 1 hour and increasing each time by a factor of 1.5, up to 16
# hours, then retries every 6 hours until 4 days have passed since the first
# failed delivery.

# WARNING: If you do not have any retry rules at all (this section of the
# configuration is non-existent or empty), Exim will not do any retries of
# messages that fail to get delivered at the first attempt. The effect will
# be to treat temporary errors as permanent. Therefore, DO NOT remove this
# retry rule unless you really don't want any retries.

# Address or Domain    Error       Retries
# -----------------    -----       -------

*                      *           F,2h,15m; G,16h,1h,1; F,3d,6h



######################################################################
#                      REWRITE CONFIGURATION                         #
######################################################################

begin rewrite

.include /etc/exim/rewrite.conf

######################################################################
#                   AUTHENTICATION CONFIGURATION                     #
######################################################################

begin authenticators

.include /etc/exim/authenticators.conf

# End of Exim configuration file

